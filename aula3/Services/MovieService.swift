//
//  MovieService.swift
//  aula3
//
//  Created by Charles França on 20/07/19.
//  Copyright © 2019 Charles França. All rights reserved.
//

import Foundation
import CoreData
import UIKit

class MovieService {
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    func GetMovies(callback completion: ([Movie]) -> ()) {
        var movies = [Movie]()
        guard let context = appDelegate?.persistentContainer.viewContext else {
            return
        }
        
        let fetchRequest = NSFetchRequest<Movie>(entityName: "Movie")
        do {
            movies = try context.fetch(fetchRequest)
            completion(movies)
        } catch {
            print(error.localizedDescription)
        }
    }
}
