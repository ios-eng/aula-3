//
//  ViewController.swift
//  aula3
//
//  Created by Charles França on 20/07/19.
//  Copyright © 2019 Charles França. All rights reserved.
//

import UIKit
import CoreData

class ViewController: UIViewController {
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    var movies = [Movie]()
    @IBOutlet weak var movieCollection: UICollectionView!
    @IBOutlet weak var tblMovies: UITableView!
    @IBAction func clearData(_ sender: Any) {
        let storyBoard : UIStoryboard = UIStoryboard(name: "loginstoryboard", bundle:nil)
        let nextViewController = storyBoard.instantiateViewController(withIdentifier: "tabLogin") as! UITabBarController
        
        let navigationController = UINavigationController(rootViewController: nextViewController)
        
        let appdelegate = UIApplication.shared.delegate as! AppDelegate
        appdelegate.window!.rootViewController = navigationController
    }
    
    
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view.
        tblMovies.dataSource = self
        tblMovies.delegate = self
        
        loadMovies { (completed) in
            movieCollection.reloadData()
            tblMovies.reloadData()
        }
        
        let serverUrl = Environment().configuration(PlistKey.ServerURL)
        print(serverUrl)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        loadMovies { (completed) in
            movieCollection.reloadData()
            tblMovies.reloadData()
        }
    }
    
    func loadMovies(completion: (Bool) -> ()){
        guard let context = appDelegate?.persistentContainer.viewContext else {
            return
        }
        
        let fetchRequest = NSFetchRequest<Movie>(entityName: "Movie")
        do {
            movies = try context.fetch(fetchRequest)
//            movieCollection.reloadData()
//            tblMovies.reloadData()
            completion(true)
        } catch {
            print(error.localizedDescription)
        }
    }

}

extension ViewController : UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return movies.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "movieCellHorizontal", for: indexPath) as! MovieCollectionViewCell
        
        let movie = movies[indexPath.row]
        cell.imgMovie.image = UIImage(named: movie.image ?? "")
        
        return cell
    }
}


extension ViewController : UITableViewDelegate, UITableViewDataSource {
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return movies.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: "tableViewCellTemplate", for: indexPath) as! TableCellTableViewCell
        
        let movie = movies[indexPath.row]
        cell.imgPoster.image = UIImage(named: movie.image ?? "")
        cell.lblName.text = movie.name
        
        return cell
    }
    
    func tableView(_ tableView: UITableView, editingStyleForRowAt indexPath: IndexPath) -> UITableViewCell.EditingStyle {
        return .none
    }
    
    func tableView(_ tableView: UITableView, editActionsForRowAt indexPath: IndexPath) -> [UITableViewRowAction]? {
        let deleteAction = UITableViewRowAction(style: .destructive, title: "DELETE") { (rowAction, indexPath) in
            self.deleteMovie(atIndexPath: indexPath, completion: { (success) -> Void in
                if success { // this will be equal to whatever value is set in this method call
                    tableView.deleteRows(at: [indexPath], with: .automatic)
                    self.movieCollection.deleteItems(at: [indexPath])
                }
            })
            
        }
        
        deleteAction.backgroundColor = #colorLiteral(red: 1, green: 0.1491314173, blue: 0, alpha: 1)
        return [deleteAction]
    }
    
    
    
    func deleteMovie(atIndexPath indexPath: IndexPath, completion: (Bool) -> ()) {
        guard let context = appDelegate?.persistentContainer.viewContext else {
            return
        }
        
        context.delete(movies[indexPath.row])
        do {
            try context.save()
            loadMovies { (completed) in
                completion(true)
            }
        } catch {
            print("Error \(error.localizedDescription)")
        }
    }
}
