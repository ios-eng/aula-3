//
//  MovieViewController.swift
//  aula3
//
//  Created by Charles França on 20/07/19.
//  Copyright © 2019 Charles França. All rights reserved.
//

import UIKit
import CoreData

class MovieViewController: UIViewController {
    @IBOutlet weak var txtMovieName: UITextField!
    @IBOutlet weak var txtMovieImage: UITextField!
    @IBOutlet weak var saveMovie: UIButton!
    
    let appDelegate = UIApplication.shared.delegate as? AppDelegate
    
    @IBAction func saveMovieAction(_ sender: Any) {
        guard let context = appDelegate?.persistentContainer.viewContext else {
            return
        }
        
        let movie = Movie(context: context)
        movie.image = txtMovieImage.text
        movie.name = txtMovieName.text
        
        do {
            try context.save()
            tabBarController?.selectedIndex = 0
            print("Saved")
        } catch {
            print(error.localizedDescription)
        }
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}
