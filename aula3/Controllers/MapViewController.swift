//
//  MapViewController.swift
//  aula3
//
//  Created by Charles França on 20/07/19.
//  Copyright © 2019 Charles França. All rights reserved.
//

import UIKit
import MapKit

class MapViewController: UIViewController {
    var locationManager = CLLocationManager()
    
    @IBAction func showUSerLocation(_ sender: Any) {
        getUserLocation()
    }
    @IBOutlet weak var map: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        locationManager.delegate = self
        locationManager.requestAlwaysAuthorization()
        
//        getUserLocation()
    }
    
    func getUserLocation() {
        guard let location = locationManager.location else {
            return
        }
        
        let centerUser = CLLocationCoordinate2D(latitude: location.coordinate.latitude, longitude: location.coordinate.longitude)
        let region = MKCoordinateRegion(center: centerUser, span: MKCoordinateSpan(latitudeDelta: 0.005, longitudeDelta: 0.005))
        
        map.setRegion(region, animated: true)
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destination.
        // Pass the selected object to the new view controller.
    }
    */

}

extension MapViewController : CLLocationManagerDelegate {
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        print(locations)
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        switch status {
        case .notDetermined:
            break
        case .restricted:
            break
        case .denied:
            break
        case .authorizedAlways:
            
            break
        case .authorizedWhenInUse:
            break
        @unknown default:
            break
        }
    }
}
